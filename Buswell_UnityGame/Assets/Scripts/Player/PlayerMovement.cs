﻿using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    //Set up variables
    public float speed = 6f;
    //private variables
    Vector3 Movement;
    Animator anim;
    Rigidbody playerRigidBody;
    int floorMask;
    float camRayLength = 100f;
    NavMeshAgent agent;
    Transform friend;
    GameObject closestEnemy;
    bool activeChar = true; //is this the character currently being controlled?

    private void Awake()
    {
        //Set up references
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        agent.enabled = false;
        playerRigidBody = GetComponent<Rigidbody>();
        friend = GameObject.FindGameObjectWithTag("Friend").transform;
    }
    private void FixedUpdate()
    {
        if (activeChar)
        {
            //Get the Raw (1, 0, -1) input for both axis
            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");

            //call functions for movement, turning and animation
            Move(h, v);
            Turning();
            Animate(h, v);
        }
        else
        {
            closestEnemy = FindClosestEnemy();

            if (closestEnemy == null)
            {
                agent.SetDestination(friend.position); //Move to friend's position
            }
            else
            {
                agent.SetDestination(rotateTarget(closestEnemy.transform.position));
            }
            
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            activeChar = !activeChar;

            if (activeChar)
            {
                agent.enabled = false;
                anim.SetBool("IsWalking", false);
            }
            else
            {
                agent.enabled = true;
                anim.SetBool("IsWalking", true);
            }
        }
    }
    void Move(float h, float v)
    {
        //Set up the Movement vector and change its length to the correct value
        Movement.Set(h, 0f, v);
        Movement = Movement.normalized * speed * Time.deltaTime;
        //Apply the movement Vector to the player's rigidbody
        playerRigidBody.MovePosition(transform.position + Movement);
    }
    void Turning()
    {
        //define a ray from the camera to the mouse's position on screen
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        //Check if the camRay has intersected with the floor quad, if it has___
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            //set the difference in the point at which the ray hit the floor and the player's position to a Vector3
            Vector3 playerToMouse = floorHit.point - transform.position;
            //Set the y component of the vector to 0 to prevent the player from leaning fowards or back
            playerToMouse.y = 0f;

            //convert the Vector3 to a quaternion
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            //change the player's rotation to the desired new rotation
            playerRigidBody.MoveRotation(newRotation);
        }
    }
    void Animate(float h, float v)
    {
        //set walking to true if h not equals zero or v not equals zero
        bool walking = h != 0f || v != 0f;

        //set the boolean in the animator control to the value of walking
        anim.SetBool("IsWalking", walking);
    }

    public GameObject FindClosestEnemy()
    {
        GameObject[] enemiesArray; //an array of gameObjects with a specific tag
        enemiesArray = GameObject.FindGameObjectsWithTag("Enemy");//function that returns all game objects with the tag "enemy"
        GameObject closest = null; //temp varr to store the currently closest enemy
        float distance = Mathf.Infinity; //create temp var for distance set to infinity
        Vector3 position = transform.position; //get my position

        foreach (GameObject enemy in enemiesArray) //iterate through the array of enemies
        {
            Vector3 diff = enemy.transform.position - position; //calcualte the difference between my and the enemies position as a vector
            float curDistance = diff.sqrMagnitude; //get the magnitude (length) of the resulting vector
            if (curDistance < distance) //if that vector is less than the current winner for closest
            {
                closest = enemy; //the closest is the current enemy now
                distance = curDistance; //the distance between me and the closest enemy is now this
            }
        }

        return closest; //spit out the closest enemy after the loop
    }

     private Vector3 rotateTarget(Vector3 target)
    {
        Vector3 relative = target - transform.position;
        relative.x *= -1;
        relative.z *= -1;
        return relative;
    }
}

