﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    //Set up public variables
    public int startingHealth = 100; //health at start of scene
    public int currentHealth; //health at any given point
    public Slider healthSlider; //Reference to the slider UI element
    public Image damageImage; //Reference tp the damage image UI element
    public AudioClip deathClip;// "one shot" audio clip for when the player loses
    public float flashSpeed = 5f; //how quickly the damaged image flashes on screen
    public Color flashColour = new Color(1f, 0.2f, 0.2f, 0.1f); //red and transparent

    //private variables
    Animator anim; //animator controller
    AudioSource playerAudio; //player's audio source
    PlayerMovement playerMovement; //reference to the previously written script
    PlayerShooting playerShooting; //reference to script written in the future
    bool isDead; //true if player is dead
    bool damaged; //true if the player has taken damage


    //called right at the beggining
    void Awake ()
    {
        //Get all the refernces
        anim = GetComponent <Animator> ();
        playerAudio = GetComponent <AudioSource> ();
        playerMovement = GetComponent <PlayerMovement> ();
        playerShooting = GetComponentInChildren <PlayerShooting> ();
        currentHealth = startingHealth;
    }

    
    void Update ()
    {
        if(damaged)
        {
            damageImage.color = flashColour;  //change the damage image to the flash color
        }
        else
        {
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime); //fade the damage image to clear over time
        }
        damaged = false;
    }


    public void TakeDamage (int amount)
    {
        damaged = true; //flash the damaged image

        currentHealth -= amount; //decrease current health by damage

        healthSlider.value = currentHealth; //change the slider 

        playerAudio.Play ();//play hurt sound

        if(currentHealth <= 0 && !isDead)//if player has 0 oir less health and is not already dead
        {
            Death ();//make them dead
        }
    }


    void Death ()
    {
        isDead = true;//set dead variable 

        //playerShooting.DisableEffects ();

        anim.SetTrigger ("Die");//play death animation

        playerAudio.clip = deathClip;//change the audio that playerAudio will play
        playerAudio.Play ();//play the new audio

        playerMovement.enabled = false; //don't let the player move
        playerShooting.enabled = false; //don't let the player shoot
    }


    public void RestartLevel ()
    {
        SceneManager.LoadScene (0);
    }
}
