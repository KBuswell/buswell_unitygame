﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElefriendMovement: MonoBehaviour
{
    Transform player;
    GameObject target;
    PlayerHealth playerHealth;
    UnityEngine.AI.NavMeshAgent nav;
    Animator anim;
    float timer = 0f;
    bool activeChar = false;
    float camRayLength = 100f;
    int floorMask;
    Vector3 Movement;
    Rigidbody myRigidBody;

    public float speed = 5f;
    public float attackDelay = 3f;
    public GameObject Stomp;
    public GameObject StompParticles;

    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        player = GameObject.FindGameObjectWithTag("Player").transform;
        anim = GetComponent<Animator>();
        anim.SetBool("IsWalking", true);
        playerHealth = player.GetComponent<PlayerHealth>();
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        myRigidBody = GetComponent<Rigidbody>();
    }


    private void FixedUpdate()
    {
        if (activeChar)
        {
            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");

            //call functions for movement, turning and animation
            Move(h, v);
            Turning();
            Animate(h, v);
            if (Input.GetButton("Fire1") && timer >= (attackDelay - 1f) && Time.timeScale != 0)
            {
                stomp();
                timer = 0f;
            }

            timer += Time.deltaTime;
        }
        else
        {
            if (target != null && nav.enabled)
            {
                nav.SetDestination(target.transform.position);
            }
            else if (playerHealth.currentHealth > 0)
            {
                nav.SetDestination(player.position);
            }
            else
            {
                nav.enabled = false;
            }

            if (timer <= 0f)
            {
                stomp();
                timer = attackDelay;
            }
            else
            {
                timer -= Time.deltaTime;
            }
        } 
        if(playerHealth.currentHealth <= 0) //check if the player is out of health
        {
            anim.SetTrigger("Die");//play death animation
            nav.enabled = false; //disable nav mesh
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            activeChar = !activeChar;

            if (activeChar)
            {
                nav.enabled = false;
                anim.SetBool("IsWalking", false);
            }
            else
            {
                nav.enabled = true;
                anim.SetBool("IsWalking", true);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            target = other.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        target = null;
    }
    private void stomp()
    {
        Instantiate(Stomp, this.transform);
        Instantiate(StompParticles, this.transform);
    }

    void Move(float h, float v)
    {
        //Set up the Movement vector and change its length to the correct value
        Movement.Set(h, 0f, v);
        Movement = Movement.normalized * speed * Time.deltaTime;
        //Apply the movement Vector to the player's rigidbody
        myRigidBody.MovePosition(transform.position + Movement);
    }
    void Turning()
    {
        //define a ray from the camera to the mouse's position on screen
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        //Check if the camRay has intersected with the floor quad, if it has___
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            //set the difference in the point at which the ray hit the floor and the player's position to a Vector3
            Vector3 playerToMouse = floorHit.point - transform.position;
            //Set the y component of the vector to 0 to prevent the player from leaning fowards or back
            playerToMouse.y = 0f;

            //convert the Vector3 to a quaternion
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            //change the player's rotation to the desired new rotation
            myRigidBody.MoveRotation(newRotation);
        }
    }
    void Animate(float h, float v)
    {
        //set walking to true if h not equals zero or v not equals zero
        bool walking = h != 0f || v != 0f;

        //set the boolean in the animator control to the value of walking
        anim.SetBool("IsWalking", walking);
    }
}
