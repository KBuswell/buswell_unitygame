﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StompDamage : MonoBehaviour
{
    float timer = .02f;
    int damage = 35;
    private void Update()
    {
        if (timer <= 0f)
        {
            Destroy(gameObject);
        }
        timer -= Time.deltaTime;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            other.gameObject.GetComponent<EnemyHealth>().TakeDamage(damage);
        }
    }

}
