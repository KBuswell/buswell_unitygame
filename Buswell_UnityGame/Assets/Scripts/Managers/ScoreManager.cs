﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score; //set up the score, make it so that all objects can access it and it is bound to the class not the instance


    Text text; //the text that would be diplayed


    void Awake ()
    {
        text = GetComponent <Text> (); //get the text component
        score = 0; //reset the score
    }


    void Update ()
    {
        text.text = "Score: " + score; //put the score onscreen in the text component
    }
}
