﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    //Set up variables
    public Transform[] targetsArray; //have multiple targets and lerp between them, Array of targets?
    
    public float smoothing = 5f;

    Vector3 offset;
    int character = 0;

    void Start()
    {
        //set offset to the initial distace between the camera and the player
        targetsArray[0] = GameObject.FindGameObjectWithTag("Player").transform;
        targetsArray[1] = GameObject.FindGameObjectWithTag("Friend").transform;
        offset = transform.position - targetsArray[character].position;
    }

    private void FixedUpdate()
    {
        
        //define the target position for the camera to be the player's current position modified by the offset
        Vector3 targetCamPos = targetsArray[character].position + offset;
        //interpolate the camera's position at the speed of smoothing between its current and target position
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            if (character == 1)
            {
                character = 0;
            }
            else
            {
                character = 1;
            }
        }
    }
}
